CREATE TABLE `user` (
    `id` int unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL,
    `name` varchar(255) NOT NULL,
    `surname` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `birthdate` timestamp NOT NULL,
    `email` varchar(255) NOT NULL,
    `country` varchar(255) NOT NULL,
    `dateCreated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `dateLastChange` DATETIME NOT NULL,
    PRIMARY KEY (`id`), UNIQUE (`username`)
);
CREATE TABLE `captcha` (
    `cid` int unsigned NOT NULL AUTO_INCREMENT,
    `captcha` varchar(255) NOT NULL,
    `timesUsed` int unsigned NOT NULL,
    PRIMARY KEY (`cid`)
);
CREATE TABLE `log` (
    `lid` int unsigned NOT NULL AUTO_INCREMENT,
    `username` varchar(255) NOT NULL,
    `captchaID` int unsigned NOT NULL,
    `status` varchar(255) NOT NULL,
    `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`lid`)
);
ALTER TABLE log
ADD FOREIGN KEY (`username`) REFERENCES `user`(`username`);

ALTER TABLE log
ADD FOREIGN KEY (`captchaID`) REFERENCES `captcha`(`cid`);





INSERT INTO captcha
(`username`, `name`, `surname`, `password`, `birthdate`, `email`, `country`, `dateCreated`, `dateLastChange`)
VALUES
("Gabriels_super_account", "Gabriel", "Puff", "root++", "2002-07-19", "puffgabriel.202@gmail.com", "Austria", "2019-03-28")



INSERT INTO captcha
(`username`, `name`, `surname`, `password`, `birthdate`, `email`, `country`, `dateCreated`, `dateLastChange`)
VALUES
("Gabriels_super_account", "Gabriel", "Puff", "root++", "2002-07-19", "puffgabriel.202@gmail.com", "Austria", "2019-03-28"),
("Test_acc1", "Hans", "Von Los", "1234", "1406-02-24", "herr.vonLos@gmail.com", "Germany", "2019-03-28"),
("Test_acc_2", "Sepp", "Hinterberger", "1234", "1884-11-02", "hinter.berger@gmail.com", "Austria", "2019-03-28");



ALTER TABLE user AUTO_INCREMENT = 1;




TUTORIAL
https://de.wikihow.com/Ein-sicheres-Login-Skript-mit-PHP-und-MySQL-erstellen

https://www.w3schools.com/howto/howto_css_signup_form.asp






